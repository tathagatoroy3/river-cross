import pygame
from random import seed
from random import randint
from config import *

seed(3)


# class of moving obstacles
class move_o(pygame.sprite.Sprite):

    def __init__(self, color, width, height):
        # call constructor
        super().__init__()

        # pass color,pos,background
        self.image = pygame.Surface([width, height])
        self.image.fill(GREEN)
        self.image.set_colorkey(WHITE)
        self.rect = self.image.get_rect()

        # import image
        self.image = pygame.image.load("ghost.png").convert_alpha()
        self.rect = self.image.get_rect()

        self.speed = 2
        self.sspeed = 2
        self.direct = 0

    def update(self):
        if self.direct == 0:
            k = randint(0, 10)
            if k >= 5:
                self.direct = 1
            elif k < 5:
                self.direct = -1
        if(self.rect.x >= 700):
            self.direct = -1
        elif(self.rect.x <= 0):
            self.direct = 1
        if (self.direct == 1):
            self.rect.x += self.speed
        elif self.direct == -1:
            self.rect.x -= self.speed
