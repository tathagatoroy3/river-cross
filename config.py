import pygame


# defining colors
BLACK = (0, 0, 0)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
BLUE = (0, 0, 128)
BROWN = (165, 42, 42)
YELLOW = (255, 255, 0)
WHITE = (255, 255, 0)

# defining fonts
pygame.font.init()
font = pygame.font.SysFont('arial', 15)
font1 = pygame.font.Font("freesansbold.ttf", 20)
