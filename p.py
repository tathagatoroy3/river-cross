import pygame
from config import *


# class pikachu that is the player
class pikachu(pygame.sprite.Sprite):

   def __init__(self, color, width, height):
        # Call the class constructor
        super().__init__()

        # pass the color,pos,and background
        self.image = pygame.Surface([width, height])
        self.image.fill(WHITE)
        self.image.set_colorkey(WHITE)

        # import image
        self.image = pygame.image.load("pika.png").convert_alpha()
        self.rect = self.image.get_rect()

     # methods/function to move left,right,down,up
   def right(self, pixels):
       self.rect.x +=  pixels 
   def left(self, pixels):
       self.rect.x -= pixels  
   def up(self, pixels):
       self.rect.y -= pixels
   def down(self, pixels):
       self.rect.y += pixels
         