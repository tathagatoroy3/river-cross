# importing and initialising pygame
import pygame
from p import pikachu
from random import seed
from random import randint
from land_enemy import land_enemy
from mov_o import move_o
from config import *

# initialise pygame
pygame.init()

# seed for random int
seed(1)

# time variables
# past temp var to store last recorded time
time_1 = 0
time_2 = 0
t_1 = 0
t_2 = 0
past = 0

# new window
size = (700, 700)
screen = pygame.display.set_mode(size)
pygame.display.set_caption("RIVER CROSS")

# all sprite list
all_sprite_list = pygame.sprite.Group()

# enemies to store obstacles sprite
# standing is the list of stationary objects
# moving is the list of moving objects
enemies = pygame.sprite.Group()
standing = []
moving = []

# player object
player = pikachu(RED, 350, 500)
player.rect.x = 350
player.rect.y = 660

# adding instance of land enemy and randomising their x coordinate
# also adding them in list and sprite group for processing
count = 0
while(count < 12):
    k = count//2 + 1
#  print(k)
    obstacle = land_enemy(BROWN, 30, 30)
    obstacle.rect.x = randint(0, 600)
    obstacle.rect.y = k*100
    all_sprite_list.add(obstacle)
    enemies.add(obstacle)
    standing.append(obstacle)
    count = count+1

# adding instance of moving river object
# randomising their start height
# storing them in sprite list group and moving list

boat = move_o(GREEN, 40, 20)
boat.rect.x = randint(0, 650)
boat.rect.y = randint(0, 80)
all_sprite_list.add(boat)
enemies.add(boat)
moving.append(boat)

boat = move_o(GREEN, 40, 20)
boat.rect.x = randint(0, 650)
boat.rect.y = randint(130, 180)
all_sprite_list.add(boat)
enemies.add(boat)
moving.append(boat)

boat = move_o(GREEN, 40, 20)
boat.rect.x = randint(0, 650)
boat.rect.y = randint(230, 280)
all_sprite_list.add(boat)
enemies.add(boat)
moving.append(boat)

boat = move_o(GREEN, 40, 20)
boat.rect.x = randint(0, 650)
boat.rect.y = randint(330, 380)
all_sprite_list.add(boat)
enemies.add(boat)
moving.append(boat)

boat = move_o(GREEN, 40, 20)
boat.rect.x = randint(0, 650)
boat.rect.y = randint(430, 480)
all_sprite_list.add(boat)
enemies.add(boat)
moving.append(boat)

boat = move_o(GREEN, 40, 20)
boat.rect.x = randint(0, 650)
boat.rect.y = randint(530, 580)
all_sprite_list.add(boat)
enemies.add(boat)
moving.append(boat)

boat = move_o(GREEN, 40, 20)
boat.rect.x = randint(0, 650)
boat.rect.y = randint(630, 680)
all_sprite_list.add(boat)
enemies.add(boat)
moving.append(boat)

# sound file
crash_sound = pygame.mixer.music.load("song.mp3")
pygame.mixer.music.play(-1)

# score global,score level
gscore_1 = 0
gscore_2 = 0
lscore_1 = 0
lscore_2 = 0

# clock for screen updates
clock = pygame.time.Clock()

# variable to indicate which player is playing
cur_player = 1

# variable to see whether players have collided
col_1 = 1
col_2 = 1

# variable to store level
level = 1

# adding player to sprite list
all_sprite_list.add(player)

# starting page for the game
start = True
while start:
    # condition for ending the loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            start = False
    screen.fill(GREEN)
    text = "WELCOME TO RIVER CROSS"
    text2 = font1.render(text, False, BLACK)
    screen.blit(text2, (200, 250))
    text = "GAME MADE BY ROY STUDIOS"
    text2 = font1.render(text, False, BLACK)
    screen.blit(text2, (200, 300))
    text = "PRESS ENTER TO START GAME"
    text2 = font1.render(text, False, BLACK)
    screen.blit(text2, (200, 350))
    keys = pygame.key.get_pressed()

    # checks if enter is pressed or not that is keys[key]==1 or not
    if keys[pygame.K_RETURN]:
        start = False
        # update
    pygame.display.flip()

    # frames per sec
    clock.tick(60)

# loop  variable and exit handling
carryOn = True


# main program loop
while carryOn:

    # condition for ending the loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            carryOn = False

    # return a dictionary of keys where 1 denotes key is pressed
    keys = pygame.key.get_pressed()

    # checks if key is pressed or not that is keys[key]==1 or not
    if keys[pygame.K_LEFT]:
        player.left(3)
    if keys[pygame.K_RIGHT]:
        player.right(3)
    if keys[pygame.K_DOWN]:
        player.down(3)
    if keys[pygame.K_UP]:
        player.up(3)
    # string to display text

    # list of collision
    hitlist = pygame.sprite.spritecollide(player, enemies, False)

    # check whether list is empty
    # code to change players when collision occurs
    # display result screen if both the players screen
    if len(hitlist) > 0:
        # player1 is is hit
        if cur_player == 1:
            col_1 = 0
            print("player 1 hit")
        # player2 is hit
        elif cur_player == 2:
            col_2 = 0
            print("player 2 hit")
        # player 1 is hit and change player
        if (col_2 == 1 and col_1 == 0 and cur_player == 1):
            player.rect.x = 350
            player.rect.y = 0
            cur_player = 2
            k = pygame.time.get_ticks()
            time_1 = k - past
            past = k
            print(" player 2  plays now")
        # round finishes as player 2 is hit
        elif (col_1 == 1 and col_2 == 0):
            gscore_1 = lscore_1
            gscore_2 = lscore_2
            player.rect.x = 350
            player.rect.y = 700
            col_2 == 0
            k = pygame.time.get_ticks()
            time_2 += k - past
            past = k
            cur_player = 1
            level = level + 1
            # result screen
            final = True
            print("harsh")
            while final:
                print("kushal")
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        final = False
                screen.fill(RED)
                text = " score of player 1 : " + str(gscore_1)
                text1 = " time taken by player 1 : " + str((time_1)/1000)
                text2 = " score of player 2 : " + str(gscore_2)
                text3 = " time taken by player 2 : " + str((time_2)/1000)
                text4 = font1.render(text, False, GREEN)
                screen.blit(text4, (250, 250))
                text4 = font1.render(text1, False, GREEN)
                screen.blit(text4, (250, 275))
                text4 = font1.render(text2, False, GREEN)
                screen.blit(text4, (250, 300))
                text4 = font1.render(text3, False, GREEN)
                screen.blit(text4, (250, 325))
                if(gscore_1 > gscore_2 or (gscore_1 == gscore_2 and
                   time_1 < time_2)):
                    text = "WINNER IS PLAYER 1"
                    text4 = font1.render(text, False, GREEN)
                    screen.blit(text4, (250, 350))
                else:
                    text = "WINNER IS PLAYER 2"
                    text4 = font1.render(text, False, GREEN)
                    screen.blit(text4, (250, 350))
                text = "PRESS N TO START NEXT ROUND"
                text4 = font1.render(text, False, GREEN)
                screen.blit(text4, (250, 580))
                text = "PRESS Q TO QUIT GAME"
                text4 = font1.render(text, False, GREEN)
                screen.blit(text4, (250, 500))
                keys = pygame.key.get_pressed()
                if keys[pygame.K_n]:
                    final = False
                elif keys[pygame.K_q]:
                    final = False
                    carryOn = False

                # update
                pygame.display.flip()

                # frames per sec

        # if both players hit next round after result page
        elif (col_1 == 0 and col_2 == 0):
            cur_player = 1
            level += 1
            player.rect.x = 350
            player.rect.y = 700
            gscore_1 = lscore_1
            gscore_2 = lscore_2
            col_1 = 1
            col_2 = 1
            time_2 = pygame.time.get_ticks()-past
            past = pygame.time.get_ticks()
            time_1 = t_1
            final = True
            print("hshs")
            # display result page
            while final:
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        final = False
                print("collision")
                screen.fill(RED)
                print("")

                text = " score of player 1 : " + str(gscore_1)
                text1 = " time taken by player 1 : " + str((time_1)/1000)
                text2 = " score of player 2 : " + str(gscore_2)
                text3 = " time taken by player 2 : " + str((time_2)/1000)
                print(" check ")
                text4 = font1.render(text, False, GREEN)
                screen.blit(text4, (250, 250))
                text4 = font1.render(text1, False, GREEN)
                screen.blit(text4, (250, 275))
                text4 = font1.render(text2, False, GREEN)
                screen.blit(text4, (250, 300))
                text4 = font1.render(text3, False, GREEN)
                screen.blit(text4, (250, 325))
                if(gscore_1 > gscore_2 or (gscore_1 == gscore_2 and
                   time_1 < time_2)):
                    text = "WINNER IS PLAYER 1"
                    text4 = font1.render(text, False, GREEN)
                    screen.blit(text4, (250, 350))
                else:
                    text = "WINNER IS PLAYER 2"
                    text4 = font1.render(text, False, GREEN)
                    screen.blit(text4, (350, 350))

                print(" hhhh")
                text = "PRESS N TO START NEXT ROUND"
                text4 = font1.render(text, False, GREEN)
                screen.blit(text4, (250, 580))
                text = "PRESS Q TO QUIT GAME"
                text4 = font1.render(text, False, GREEN)
                screen.blit(text4, (250, 500))
                keys = pygame.key.get_pressed()
                if keys[pygame.K_n]:
                    final = False
                elif keys[pygame.K_q]:
                    final = False
                    carryOn = False
                print(" hjgj ")
                # update
                pygame.display.flip()

    # if current player reaches end change player
    # if current player is 1 player changes to 2
    if(cur_player == 1 and player.rect.y <= 0):
        cur_player = 2
        player.rect.x = 350
        player.rect.y = 0
        print("player 1 finishes level ")
        k = pygame.time.get_ticks()
        time_1 = k - past
        past = k
        print("player 2 plays now")
    # if current player is 2, result page is diplayed
    # and player 1 starts next level
    elif(cur_player == 2 and player.rect.y >= 690):
        cur_player = 1
        player.rect.x = 350
        player.rect.y = 700
        k = pygame.time.get_ticks()
        time_2 += k - past
        past = k
        level += 1
        col_1 = 1
        col_2 = 1
        final = True
        # result screen
        while final:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    final = False
            screen.fill(RED)
            print("bkkk")
            text = " score of player 1 : " + str(lscore_1)
            text1 = " time taken by player 1 : " + str((time_1)/1000)
            text2 = " score of player 2 : " + str(lscore_2)
            text3 = " time taken by player 2 : " + str((time_2)/1000)
            print("hjhh")
            text4 = font1.render(text, False, GREEN)
            screen.blit(text4, (250, 250))
            text4 = font1.render(text1, False, GREEN)
            screen.blit(text4, (250, 275))
            text4 = font1.render(text2, False, GREEN)
            screen.blit(text4, (250, 300))
            text4 = font1.render(text3, False, GREEN)
            screen.blit(text4, (250, 325))
            print("ghgj")
            if(gscore_1 > gscore_2 or (gscore_1 == gscore_2 and
               time_1 < time_2)):
                text = "WINNER IS PLAYER 1"
                text4 = font1.render(text, False, GREEN)
                screen.blit(text4, (250, 350))
            else:
                text = "WINNER IS PLAYER 2"
                text4 = font1.render(text, False, GREEN)
                screen.blit(text4, (250, 350))
            print(" ghg ")
            text = "PRESS N TO START NEXT ROUND"
            text4 = font1.render(text, False, GREEN)
            screen.blit(text4, (250, 580))
            text = "PRESS Q TO QUIT GAME"
            text4 = font1.render(text, False, GREEN)
            screen.blit(text4, (250, 500))
            keys = pygame.key.get_pressed()
            if keys[pygame.K_n]:
                final = False
            elif keys[pygame.K_q]:
                final = False
                carryOn = False

            # update
            pygame.display.flip()

    # player does not go outside of screen
    if(player.rect.x > 700):
        player.rect.x = 700
    elif(player.rect.y < 0):
        player.rect.y = 0
    # main logic
    # update sprite
    all_sprite_list.update()

    # update speed accoording to level
    for i in range(7):
        if cur_player == 1:
            moving[i].speed = moving[i].sspeed + level - 1
        else:
            moving[i].speed = moving[i].sspeed + level - 1

    # filling the screen with rivers
    screen.fill(BLUE)

    # display score
    # calculate score
    if(cur_player == 1):
        lscore_1 = 0
        # score due to moving obstacles
        for i in range(7):
            if player.rect.bottom < moving[i].rect.top:
                lscore_1 += 10
        # score due to standing obstacles
        for i in range(12):
            if player.rect.bottom < standing[i].rect.top:
                lscore_1 += 5
        k = pygame.time.get_ticks()
        t_1 = k - past
        # printing time score,player and level
        text4 = "TIME : " + str((t_1)/1000)
        text5 = font.render(text4, False, GREEN)
        screen.blit(text5, (500, 70))
        text = "PLAYER1 LEVEL: " + str(level)
        text1 = font.render(text, False, GREEN)
        text2 = "SCORE : " + str(lscore_1)
        text3 = font.render(text2, False, GREEN)
        screen.blit(text1, (500, 50))
        screen.blit(text3, (500, 20))

    elif(cur_player == 2):
        lscore_2 = 0

        for i in range(7):
            # points to moving object
            if player.rect.top > moving[i].rect.bottom:
                lscore_2 += 10
        for i in range(12):
            # points due to standing objects
            if player.rect.top > standing[i].rect.bottom:
                lscore_2 += 5
        k = pygame.time.get_ticks()
        t_2 = k - past
        # printing time , level ,score current player
        text4 = "TIME : " + str((t_2)/1000)
        text5 = font.render(text4, False, GREEN)
        screen.blit(text5, (500, 70))
        text = "PLAYER2 LEVEL: " + str(level)
        text1 = font.render(text, False, GREEN)
        text2 = "SCORE : " + str(lscore_2)
        text3 = font.render(text2, False, GREEN)
        screen.blit(text1, (500, 50))
        screen.blit(text3, (500, 20))

    # partitions
    pygame.draw.rect(screen, WHITE, (0, 100, 700, 30), 0)
    pygame.draw.rect(screen, WHITE, (0, 200, 700, 30), 0)
    pygame.draw.rect(screen, WHITE, (0, 300, 700, 30), 0)
    pygame.draw.rect(screen, WHITE, (0, 400, 700, 30), 0)
    pygame.draw.rect(screen, WHITE, (0, 500, 700, 30), 0)
    pygame.draw.rect(screen, WHITE, (0, 600, 700, 30), 0)

    # draw sprite
    all_sprite_list.draw(screen)

    # update
    pygame.display.flip()

    # frames per sec
    clock.tick(60)
